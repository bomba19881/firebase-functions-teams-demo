const functions = require('firebase-functions');
const axios = require('axios');
const admin = require('firebase-admin');
admin.initializeApp();


exports.notifyNewUser = functions.auth.user().onCreate((user) => {

    // Define variables to be used later
    const webHook = "https://outlook.office.com/webhook/...";
    const curDate = new Date();

    // Define Content send to MSFT Teams
    var card = {
        "type": "message",
        "attachments": [
            {
                "contentType": "application/vnd.microsoft.card.adaptive",
                "contentUrl": null,
                "content": {
                    "type": "AdaptiveCard",
                    "body": [
                        {
                            "type": "TextBlock",
                            "size": "Medium",
                            "weight": "Bolder",
                            "text": "New User Created"
                        },
                        {
                            "type": "FactSet",
                            "facts": [
                                {
                                    "title": "Create Date",
                                    "value": curDate.toISOString()
                                },
                                {
                                    "title": "UUID",
                                    "value": user.uid
                                }
                            ]
                        }
                    ],
                    "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
                    "version": "1.3"
                }
            }
        ]
    };

    // POST to Webhook
    axios.post(webHook, card)
        .catch(error => {
            console.error(error)
        });

});
